package com.ghostedghost.donatorprefixes;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import net.md_5.bungee.api.ChatColor;

public class MainCommand implements CommandExecutor {
	private PrefixHandler plugin;
	SubCommands subCmds;

	public MainCommand(PrefixHandler passedPlugin) {
		subCmds = new SubCommands(passedPlugin);
		plugin = passedPlugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length != 0) {

			// Version
			if (args[0].equalsIgnoreCase("version"))
				if (sender.hasPermission("DonatorPrefixes.version")) { // Does player have permission?
					subCmds.executeVersionCommand(sender); // Execute command.
				} else
					sender.sendMessage(plugin.getConfig().getString("Invalid permissions msg")); // Player has invalid
																									// permissions.

			// Reload
			if (args[0].equalsIgnoreCase("reload"))
				if (sender.hasPermission("DonatorPrefixes.reload")) { // Does player have permission?
					subCmds.executeReloadCommand(sender); // Execute command.
				} else
					sender.sendMessage(plugin.getConfig().getString("Invalid permissions msg")); // Player has invalid
																									// permissions.

			// Default message send when executing the main command.
		} else {
			Boolean hasAvailibleUpdate = false;
			try {
				hasAvailibleUpdate = plugin.updater.checkForUpdates();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sender.sendMessage(ChatColor.YELLOW+ "------------------" );
			sender.sendMessage(ChatColor.GOLD+ "Donator Prefixes" );
			sender.sendMessage(ChatColor.GRAY+ "Version: " +plugin.getDescription().getVersion());
			sender.sendMessage(ChatColor.GRAY+ "Api version: " +plugin.getDescription().getAPIVersion());
			if (hasAvailibleUpdate) {
				sender.sendMessage(ChatColor.YELLOW+ "" +ChatColor.ITALIC+ "A new update is availible." );
			}
			sender.sendMessage("");
			sender.sendMessage(ChatColor.GRAY+ "/dprefixes reload" );
			sender.sendMessage(ChatColor.YELLOW+ "------------------" );
		}
		return true;
	}
}