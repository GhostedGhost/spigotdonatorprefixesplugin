package com.ghostedghost.donatorprefixes;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import org.bukkit.plugin.java.JavaPlugin;

public class UpdateChecker {

    private PrefixHandler plugin;
    private int resourceId = 76010;
    private URL checkURL;
    private String newVersion = "";

    public UpdateChecker(PrefixHandler passedPlugin) {
    	plugin = passedPlugin;
    	
        this.newVersion = plugin.getDescription().getVersion();
        
        try {
            this.checkURL = new URL("https://api.spigotmc.org/legacy/update.php?resource=" + resourceId);
        } catch (MalformedURLException e) {
        	
        }
    }
 
    public int getProjectID() {
        return resourceId;
    }
 
    public JavaPlugin getPlugin() {
        return plugin;
    }
 
    public String getLatestVersion() {
        return newVersion;
    }
 
    public String getResourceURL() {
        return "https://www.spigotmc.org/resources/" + resourceId;
    }
 
    public boolean checkForUpdates() throws Exception {
        URLConnection con = checkURL.openConnection();
        this.newVersion = new BufferedReader(new InputStreamReader(con.getInputStream())).readLine();
        return !plugin.getDescription().getVersion().equals(newVersion);
    }

}