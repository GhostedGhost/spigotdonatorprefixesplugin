package com.ghostedghost.donatorprefixes;

import org.bukkit.command.CommandSender;
import net.md_5.bungee.api.ChatColor;

public class SubCommands {
	private PrefixHandler plugin;

	public SubCommands(PrefixHandler passedPlugin) {
		plugin = passedPlugin;
	}

	// Version
	public void executeVersionCommand(CommandSender sender) {
		// Send plugin version
		sender.sendMessage(
				ChatColor.GRAY + "[DonatorPrefixes] "+ChatColor.WHITE+"Version: "+ plugin.getDescription().getVersion());
		// Send API version
		sender.sendMessage(ChatColor.GRAY + "[DonatorPrefixes] "+ChatColor.WHITE+"Using Spigot Api version: "+ ChatColor.WHITE
				+ plugin.getDescription().getAPIVersion());
	}

	// Reload
	public void executeReloadCommand(CommandSender sender) {
		plugin.reloadPlugin();
		sender.sendMessage(ChatColor.GRAY + "[DonatorPrefixes] " + ChatColor.WHITE + "Plugin has reloaded.");
	}
}
