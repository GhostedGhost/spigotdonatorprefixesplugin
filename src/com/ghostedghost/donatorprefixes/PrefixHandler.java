package com.ghostedghost.donatorprefixes;

import java.util.List;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;
import net.md_5.bungee.api.ChatColor;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class PrefixHandler extends JavaPlugin implements Listener {
	public FileConfiguration config = this.getConfig();
    public UpdateChecker updater = null;
	public String configVersion = "1.1.2";
	public Boolean hasAvailibleUpdate = false;

	// Fired when plugin is first enabled
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
		this.getCommand("DonatorPrefixes").setExecutor(new MainCommand(this));
		updater = new UpdateChecker(this);
		Bukkit.getServer().getConsoleSender().sendMessage("[DonatorPrefixes] Plugin DonatorPrefixes has been enabled!");
		saveDefaultConfig();
		autoUpdateDonatorStatus(); // Start auto updater
		checkForUpdates();
	}

	// Fired when plugin is disabled
	@Override
	public void onDisable() {

	}

	private void checkForUpdates() {
		Logger logger = this.getLogger();

		// Check for plugin updates.
	    try {
	        if (updater.checkForUpdates())
	            logger.info(ChatColor.AQUA+ "A new version is availible! " + updater.getLatestVersion() + " download: " + updater.getResourceURL());
            logger.info(ChatColor.AQUA+ "Your version: " + this.getDescription().getVersion());
            logger.info(ChatColor.AQUA+ "Download it at: " + updater.getResourceURL());
	    } catch (Exception e) {
	        logger.warning("Could not check for updates! Stacktrace:");
	        e.printStackTrace();
	    }
		
		// Check for config updates.											   v this here is to check for legacy configs. (Which definetely need updates)
		if ( !config.getString("Config version").equalsIgnoreCase(configVersion) | config.contains("Check for updates?") ) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				public void run() {
					logger.info(ChatColor.AQUA+ "A new config is out. Please delete your config file and re-create it with '/deprefixes reload' to make use of new features!" );
					logger.info(ChatColor.AQUA+ "Just remember to create a backup. ;)" );
					logger.info(ChatColor.AQUA+ "Everything will keep working as intended even if you don't update your config. :)" );
				}
			}, 15L);
		}
	}

	public void reloadPlugin() {
		saveDefaultConfig(); // Re-generate the config.yml in case it was intentionally deleted.
		reloadConfig(); // Reload the config
		config = getConfig(); // Load the reloaded config into the plugin's memory.
		updateAllUsersDonarStatus(); // Reload all donator statuses to update any changes.
	}

	// Update a player's donator status on login.
	@EventHandler
	public void playerLoginEvent(PlayerLoginEvent event) {
		updateUserDonarStatus(event.getPlayer());
	}

	// Update all online user's donator status.
	public void updateAllUsersDonarStatus() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			updateUserDonarStatus(player);
		}
	}

	// Update a specific user's donator status and their prefix.
	public void updateUserDonarStatus(Player player) {
		formatPlayerPrefix(player, getUserDonatorStatus(player));
	}

	// Check what group of donators this player is in // Returns null if the player
	// isn't a donator.
	@SuppressWarnings("unchecked")
	public String getUserDonatorStatus(Player player) {
		// Get all groups this player is in.
		PermissionUser user = PermissionsEx.getUser(player);
		List<String> groups = user.getParentIdentifiers("World");

		// Check if any of these groups is a valid Donator group.
		// For each group this player is in...
		for (int i = 0; i < groups.size(); i++) {
			// Check if it is a valid donator group.
			for (String rank : (List<String>) config.getList("Donator ranks")) {
				// If this player is in this donator group...
				if (rank.equalsIgnoreCase(groups.get(i))) {
					// Return the name of the donator group that this player belongs to.
					return rank;
				}
			}
		}
		// This player is not in any Donator group.
		return null;
	}

	// What this plugin is all about... appending a donator prefix to the user's
	// name while keeping their ranked prefix.
	@SuppressWarnings("unchecked")
	private void formatPlayerPrefix(Player player, String donatorRank) {
		PermissionUser user = PermissionsEx.getUser(player);

		// Return this user's prefix to it's original rank.
		// this is in case donator status was removed,
		// and to prevent formatting bugs.
		resetUserPrefix(player);

		// If this player is a donator...
		if (donatorRank != null) {

			String prefix = null;
			String dPrefix = config.getString("Donator prefixes." + donatorRank + ".Prefix");
			String rankPrefix = user.getPrefix();

			// Format prefix based on config settings.
			if (config.getBoolean("Donator prefix before rank") && config.getBoolean("Donator prefix after rank"))
				prefix = dPrefix + rankPrefix + dPrefix;
			else if (config.getBoolean("Donator prefix before rank"))
				prefix = dPrefix + rankPrefix;
			else if (config.getBoolean("Donator prefix after rank"))
				prefix = rankPrefix + dPrefix;

			// set this user's newly formatted prefix for all worlds
			for (String world : (List<String>) config.getList("World names")) {
				user.setPrefix(prefix, world);
			}
		}
	}

	// Reset a user's prefix so it only contains their current rank.
	@SuppressWarnings("unchecked")
	private void resetUserPrefix(Player player) {
		PermissionUser user = PermissionsEx.getUser(player);

		// Reset this user's prefixes for all worlds (So the user's prefix is now just
		// their rank)
		for (String world : (List<String>) config.getList("World names")) {
			user.setPrefix("", world);
		}
	}

	// Automatically update all user's donator status periodically.
	private void autoUpdateDonatorStatus() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				updateAllUsersDonarStatus();
			}
		}, 0L, config.getLong("Update donator status every * seconds") * 20);
	}
}
